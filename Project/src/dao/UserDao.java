package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	//ログイン画面
	public User findByLoginInfo(String loginId,String password) {
		Connection conn = null;
		String pass = magic(password);
			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, pass);
				ResultSet rs = pStmt.executeQuery();

				if(!rs.next()) {
					return null;
				}

				String loginIdDate = rs.getString("login_id");
				String nameDate = rs.getString("name");
				return new User(loginIdDate,nameDate);

			}catch (SQLException e){
				e.printStackTrace();
				return null;
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}
	//ユーザ一覧
	public List<User> findAll(){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id !=1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birthDate = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            String updateDate = rs.getString("update_date");
	            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

	            userList.add(user);
	        }
		 } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
		return userList;
	}



	//ユーザ検索
	public List<User> findSearch(String loginIdP, String nameP,String startP,String endP){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id !=1";

			if(!loginIdP.equals("")) {
				sql += " and login_id = '" + loginIdP + "'";
			}
			if(!nameP.equals("")) {
				sql += " and name like '" + "%" + nameP + "%" + "'";
			}
			if(!startP.equals("")) {
				sql += " and birth_date >= '" + startP + "'";
			}
			if(!endP.equals("")) {
				sql += " and birth_date <= '" + endP + "'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birthDate = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            String updateDate = rs.getString("update_date");
	            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

	            userList.add(user);
	        }
		 } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
		return userList;
	}

	//ユーザ名検索
	/*
	public List<User> findName(String name) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE name LIKE ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, "%" + name + "%");
				ResultSet rs = pStmt.executeQuery();


				while(rs.next()) {
					int id = rs.getInt("id");
		            String loginId = rs.getString("login_id");
		            String Name = rs.getString("name");
		            Date birthDate = rs.getDate("birth_date");
		            String password = rs.getString("password");
		            String createDate = rs.getString("create_date");
		            String updateDate = rs.getString("update_date");
		            User user = new User(id, loginId, Name, birthDate, password, createDate, updateDate);

				userList.add(user);
					}
				}catch (SQLException e){
				e.printStackTrace();
				return null;
				}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return userList;
	}
	*/

	//ユーザ新規登録
	public void create(String loginId,String password,String name,String birthDate){
		Connection conn = null;
		String pass = magic(password);
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO user (login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now())";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, pass);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.executeUpdate();

	}catch (SQLException e){
		e.printStackTrace();
	}finally{
		if(conn != null) {
			try {
				conn.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
	//ユーザ新規登録（条件）
	public User findLoginId(String loginId) {
		Connection conn = null;
			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE login_id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				ResultSet rs = pStmt.executeQuery();

				if(!rs.next()) {
					return null;
				}

				String loginIdDate = rs.getString("login_id");

				return new User(loginIdDate);

			}catch (SQLException e){
				e.printStackTrace();
				return null;
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}
	//ユーザ詳細
	public User Details(String id) {
		Connection conn = null;
			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user WHERE id = ?";
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				ResultSet rs = pStmt.executeQuery();

				if(!rs.next()) {
					return null;
				}


				int id2 = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birthDate = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            String updateDate = rs.getString("update_date");
	            User user = new User(id2, loginId, name, birthDate, password, createDate, updateDate);
				return user;

			}catch (SQLException e){
				e.printStackTrace();
				return null;
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
	//ユーザ更新
			public void update1(String name,String birthDate,String id) {
				Connection conn = null;
				try {
					conn = DBManager.getConnection();
					String sql = "UPDATE user SET name = ?,birth_date = ?,update_date = now() WHERE id = ?";
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, name);
					pStmt.setString(2, birthDate);
					pStmt.setString(3, id);
					pStmt.executeUpdate();

			}catch (SQLException e){
				e.printStackTrace();
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
			public void update2(String password,String name,String birthDate,String id) {
				Connection conn = null;
				String pass = magic(password);
				try {
					conn = DBManager.getConnection();
					String sql = "UPDATE user SET password= ?,name = ?,birth_date = ?,update_date = now() WHERE id = ?";
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, pass);
					pStmt.setString(2, name);
					pStmt.setString(3, birthDate);
					pStmt.setString(4, id);
					pStmt.executeUpdate();

			}catch (SQLException e){
				e.printStackTrace();
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
			public void delete(String id,String loginId) {
				Connection conn = null;

				try {
					conn = DBManager.getConnection();
					String sql = "DELETE FROM user WHERE id = ?";
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, id);
					pStmt.executeUpdate();

			}catch (SQLException e){
				e.printStackTrace();
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
			public String magic(String password) {
				//ハッシュを生成したい元の文字列
				String source = password ;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = null;
				try {
					bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				String result = DatatypeConverter.printHexBinary(bytes);
				System.out.println(result);
				return result;
			}
	}
