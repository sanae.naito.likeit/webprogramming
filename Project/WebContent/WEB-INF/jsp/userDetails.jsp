<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

    <ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active">${userInfo.name}　さん</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="LogoutServlet">ログアウト</a>
  </li>
    </ul>

    <body>
    <div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <h1>ユーザ情報詳細参照</h1>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>
  <form class = "form-details" action = "UserDetailsServlet">
  <div class="row">
      <div class="col">
      </div>
      <div class="col-md-auto">
        <strong>ログインID</strong>　　　　　　　${user.loginId}
        <br>
        <br>
        <strong>ユーザ名</strong>　　　　　　　　${user.name}
        <br>
        <br>
        <strong>生年月日</strong>　　　　　　　　${user.birthDate}
        <br>
        <br>
        <strong>登録日時</strong>　　　　　　　　${user.createDate}
        <br>
        <br>
        <strong>更新日時</strong>　　　　　　　　${user.updateDate}
        <br>
        <br>
      </div>
      <div class="col">
      </div>
    </div>
    </form>
   <div class="row">
    <div class="col">
    </div>
    <div class="col">
    </div>
    <div class="col">
    <br>
        <a class="nav-link" href="UserListServlet">戻る</a>
    </div>
   </div>
    </div>
    </body>
</html>