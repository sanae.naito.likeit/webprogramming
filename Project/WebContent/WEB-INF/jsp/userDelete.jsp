<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザ削除確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

    <ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active">${userInfo.name}　さん</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="LogoutServlet">ログアウト</a>
  </li>
    </ul>

    <body>
    <form class = "form-delete" action = "UserDeleteServlet" method = "post">
    <div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <h1>ユーザ削除確認</h1>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>

	<input type="hidden" name="id" value="${user.id}">
     ログインID　:　${user.loginId}　を本当に削除してよろしいでしょうか。


  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <a class="btn btn-light btn- m" href="UserListServlet">キャンセル</a>
        <button type="submit" class="btn btn-light btn-sm">　　OK　　</button>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>
    </div>
    </form>
    </body>
</html>