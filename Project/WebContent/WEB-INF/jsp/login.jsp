<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
    <body>
     <div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <div class="container">
  <div class="row align-items-start">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <h1>ログイン画面</h1>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>
  <form class = "form-signin" action = "LoginServlet" method = "post">
  <div class="row align-items-center">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <strong>ログインID</strong>　　　<input type="text" name="loginId" id="inputLoginId">
        <br>
        <br>
        <strong>パスワード</strong>　　　<input type="password" name="password" id="inputPassword">
    </div>
    <div class="col">
    </div>
  </div>
  <div class="row align-items-end">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <button type="submit" class="btn btn-link">ログイン</button>
    </div>
    <div class="col">
    </div>
    </div>
  	</form>
  	</div>
  	</div>
    </body>
</html>
