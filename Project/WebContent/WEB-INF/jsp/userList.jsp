<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザ一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

    <ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active">${userInfo.name}　さん</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="LogoutServlet">ログアウト</a>
  </li>
    </ul>

    <body>
    <form class = "form-list" action = "UserListServlet" method = "post">
    <div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <h1>ユーザ一覧</h1>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>
  <div class="row">
    <div class="col">
    </div>
    <div class="col">
    </div>
    <div class="col">
        <a href="UserCreateServlet">新規登録</a>
    </div>
  </div>
  <div class="row">
      <div class="col">
      </div>
      <div class="col-md-auto">
        <strong>ログインID</strong>　<input type="text" name="login-id" id="login-id">
        <br>
        <br>
        <strong>ユーザ名</strong>　　<input type="text" name="user-name" id="user-name">
        <br>
        <br>
        <strong>生年月日</strong><input type="date" name="date-start" id="date-start" max="9999-12-31">　～　<input type="date" name="date-end" id="date-end" max="9999-12-31">
      </div>
      <div class="col">
      </div>
    </div>
  <div class="row">
    <div class="col">
    </div>
    <div class="col">
    </div>
    <div class="col">
        <br>
        <button type="submit" class="btn btn-link">検索</button>
    </div>
　 </div>
    </div>
     </form>
    <table class="table table-bordered">
    <thead class="thead-dark">
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
					<c:choose>
					<c:when test = "${userInfo.loginId == 'admin'}">
					   <a class="btn btn-primary" href="UserDetailsServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
					</c:when>
					<c:when test = "${userInfo.loginId == user.loginId}">
					   <a class="btn btn-primary" href="UserDetailsServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					</c:when>
					<c:otherwise>
					   <a class="btn btn-primary" href="UserDetailsServlet?id=${user.id}">詳細</a>
					</c:otherwise>
					</c:choose>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
  		</table>
    </body>
</html>