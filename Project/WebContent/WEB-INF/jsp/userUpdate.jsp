<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

    <ul class="nav justify-content-end">
  <li class="nav-item">
    <a class="nav-link active">${userInfo.name}　さん</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="LogoutServlet">ログアウト</a>
  </li>
    </ul>

    <body>
    <div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>
    <div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <br>
        <h1>ユーザ情報更新</h1>
        <br>
        <br>
    </div>
    <div class="col">
    </div>
  </div>
  <form class = "form-update" action = "UserUpdateServlet" method = "post">
  <div class="row">
      <div class="col">
      </div>
      <div class="col-md-auto">
        <%-- hiddenは存在するけど、画面には表示されない、透明マントみたいな感じ--%>
      	<input type="hidden" name="id" value="${user.id}">
        <strong>ログインID</strong>　　　　　　　${user.loginId}
        <br>
        <br>
        <strong>パスワード</strong>　　　　　　　<input type="password" name="password">
        <br>
        <br>
        <strong>パスワード（確認）</strong>　　　<input type="password" name="password2">
        <br>
        <br>
        <strong>ユーザ名</strong>　　　　　　　　<input type="text" name="user-name" value="${user.name}">
        <br>
        <br>
        <strong>生年月日</strong>　　　　　　　　<input type="date" name="birthDate" value="${user.birthDate}">
        <br>
        <br>
      </div>
      <div class="col">
      </div>
    </div>
  <div class="row">
    <div class="col">
    </div>
    <div class="col-md-auto">
        <br>
        <button type="submit" class="btn btn-light">更新</button>
    </div>
    <div class="col">
    </div>
　 </div>
	</form>
   <div class="row">
    <div class="col">
        <br>
        <a class="nav-link" href="UserListServlet">戻る</a>
    </div>
    <div class="col">
    </div>
    <div class="col">
    </div>
   </div>
    </div>
    </body>
</html>