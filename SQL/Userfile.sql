﻿create database userfile default character set utf8;
use userfile;
create table user;
create table user(
	id  serial primary key auto_increment,
	login_id varchar(255) unique not null,
	name varchar(255) not null,
	birth_date date not null,
	password varchar(255) not null,
	create_date datetime not null,
	update_date datetime not null
);
